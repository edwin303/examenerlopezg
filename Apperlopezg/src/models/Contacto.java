/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author Comtech
 */
public class Contacto {
    private int id;
    private String nombre;
    private String apellido;
    private ArrayList<Numero> numeros;

    public Contacto() {
        numeros = new ArrayList();
    }

    public Contacto(int id, String nombre, String apellido, ArrayList<Numero> numeros) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.numeros = numeros;
    }

    public Contacto(int id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        numeros = new ArrayList();
    }

    public int getId() {
        return id;
    }

    public void setId(int codigo) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public ArrayList<Numero> getNumeros() {
        return numeros;
    }

    public void setNumeros(ArrayList<Numero> numeros) {
        this.numeros = numeros;
    }
}
